json.extract! product, :id, :name, :type_product, :size, :distributor_id, :created_product, :gross_amount, :sell_amount, :available_product, :created_at, :updated_at
json.url product_url(product, format: :json)
