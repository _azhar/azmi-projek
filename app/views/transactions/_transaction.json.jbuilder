json.extract! transaction, :id, :distributor_id, :product_id, :total, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
