json.extract! user, :id, :name, :adress, :telephone, :username, :password, :status, :access, :created_at, :updated_at
json.url user_url(user, format: :json)
