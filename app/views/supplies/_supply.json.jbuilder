json.extract! supply, :id, :distributor_id, :product_id, :total, :created_at, :updated_at
json.url supply_url(supply, format: :json)
