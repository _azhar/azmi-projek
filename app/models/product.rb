class Product < ActiveRecord::Base
  belongs_to :distributor
  has_many :products
  validates_presence_of :name, :distributor_id
end
