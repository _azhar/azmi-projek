class Distributor < ActiveRecord::Base
  has_many :products
  has_many :supplies
  has_many :transactions
end
