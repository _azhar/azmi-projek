class Transaction < ActiveRecord::Base
  belongs_to :distributor
  belongs_to :product

  validates_presence_of :distributor_id, :product_id
end
