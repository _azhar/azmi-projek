class ReportsController < ApplicationController

  YEAR = [
    {name: '2018', value: '2018'},
    {name: '2019', value: '2019'},
    {name: '2020', value: '2020'}
  ]

  MONTHS = [
    {name: 'Pilih Bulan', value: ''},
    {name: 'Januari', value: '01'},
    {name: 'Februari', value: '02'},
    {name: 'Maret', value: '03'},
    {name: 'April', value: '04'},
    {name: 'Mei', value: '05'},
    {name: 'Juni', value: '06'},
    {name: 'Juli', value: '07'},
    {name: 'Agustus', value: '08'},
    {name: 'September', value: '09'},
    {name: 'Oktober', value: '10'},
    {name: 'November', value: '11'},
    {name: 'Desember', value: '12'}
  ]

  def index
    if params[:date].present? && params[:date][:year]
      params[:year] = params[:date][:year]
    end

    if params[:date].present? && params[:date][:month]
      params[:month] = params[:date][:month]
    end

    if params[:year].present? && params[:month].present?
      @transactions = Transaction.where('extract(year from created_at) = ?', params[:year]).where('extract(month from created_at) = ?', params[:month])
      @supplies = Supply.where('extract(year from created_at) = ?', params[:year]).where('extract(month from created_at) = ?', params[:month])
    end

    if params[:year].present? && params[:month].blank?
      @transactions = Transaction.where('extract(year from created_at) = ?', params[:year])
      @supplies = Supply.where('extract(year from created_at) = ?', params[:year])
    end

    if params[:year].blank? && params[:month].blank?
      @transactions = Transaction.all
      @supplies = Supply.all
    end

    @years = YEAR
    @months = MONTHS

    @total_transactions = @transactions.sum(:total)
    @total_price_transactions = @transactions.sum(:total_price)
    @total_supplies = @supplies.sum(:total)
    @total_price_supplies = 0
    @supplies.each do |supply|
      @total_price_supplies += ( supply.product.gross_amount * supply.total )
    end
    
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "reoprts-#{Time.now.strftime('%Y-%m-%d')}",
               orientation: "landscape"
      end
    end
  end

end
