class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :check_current_user!

  def check_current_user!
    unless current_user && current_user.is_active?
      reset_session
    end
  end
end
