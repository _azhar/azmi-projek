# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.destroy_all

User.create(
  email: 'admin@example.com', 
  name: 'admin',
  telephone: '08123',
  adress: 'alamat', 
  password: 'password', 
  password_confirmation: 'password',
  access: "admin",
  status: true
)

User.create(
  email: 'kasir@example.com', 
  name: 'kasir',
  telephone: '08123',
  adress: 'alamat', 
  password: 'password', 
  password_confirmation: 'password',
  access: "kasir",
  status: true
)

User.create(
  email: 'kasir_banned@example.com', 
  name: 'kasir kena hukum',
  telephone: '0812',
  adress: 'alamat', 
  password: 'password', 
  password_confirmation: 'password',
  access: "admin",
  status: false
)