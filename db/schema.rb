# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181024174811) do

  create_table "distributors", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "adress",     limit: 255
    t.string   "telephone",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "type_product",      limit: 255
    t.string   "size",              limit: 255
    t.integer  "distributor_id",    limit: 4
    t.datetime "created_product"
    t.integer  "gross_amount",      limit: 4
    t.integer  "sell_amount",       limit: 4
    t.decimal  "ppn",                           precision: 10
    t.decimal  "discount",                      precision: 10
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "available_product", limit: 4
  end

  create_table "supplies", force: :cascade do |t|
    t.integer  "distributor_id", limit: 4
    t.integer  "product_id",     limit: 4
    t.integer  "total",          limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "distributor_id", limit: 4
    t.integer  "product_id",     limit: 4
    t.integer  "total",          limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "discount",       limit: 4
    t.float    "ppn",            limit: 24
    t.float    "total_price",    limit: 24
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "adress",                 limit: 255
    t.string   "telephone",              limit: 255
    t.string   "username",               limit: 255
    t.string   "password",               limit: 255
    t.boolean  "status"
    t.string   "access",                 limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
