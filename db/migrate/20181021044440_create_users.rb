class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :adress
      t.string :telephone
      t.string :username
      t.string :password
      t.boolean :status
      t.string :access

      t.timestamps null: false
    end
  end
end
