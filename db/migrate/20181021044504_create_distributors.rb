class CreateDistributors < ActiveRecord::Migration
  def change
    create_table :distributors do |t|
      t.string :name
      t.string :adress
      t.string :telephone

      t.timestamps null: false
    end
  end
end
