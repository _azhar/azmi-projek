class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :type_product
      t.string :size
      t.integer :distributor_id
      t.datetime :created_product
      t.integer :gross_amount
      t.integer :sell_amount
      t.decimal :ppn
      t.decimal :discount

      t.timestamps null: false
    end
  end
end
