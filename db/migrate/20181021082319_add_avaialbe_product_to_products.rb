class AddAvaialbeProductToProducts < ActiveRecord::Migration
  def change
    add_column :products, :available_product, :integer
    add_column :transactions, :discount, :integer
    add_column :transactions, :ppn, :decimal
    add_column :transactions, :total_price, :float
  end
end
