class CreateSupplies < ActiveRecord::Migration
  def change
    create_table :supplies do |t|
      t.integer :distributor_id
      t.integer :product_id
      t.integer :total

      t.timestamps null: false
    end
  end
end
