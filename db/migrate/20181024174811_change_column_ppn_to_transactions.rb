class ChangeColumnPpnToTransactions < ActiveRecord::Migration
  def change
    change_column :transactions, :ppn, :float
  end
end
